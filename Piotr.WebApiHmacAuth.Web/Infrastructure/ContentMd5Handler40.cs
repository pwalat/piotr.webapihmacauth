﻿using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Piotr.WebApiHmacAuth.Web.Infrastructure
{
    public class ContentMd5Handler40 : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            return base.SendAsync(request, cancellationToken)
                       .ContinueWith(task =>
                                         {
                                             var response = task.Result;
                                             MD5 md5 = MD5.Create();
                                             var content = response.Content.ReadAsByteArrayAsync().Result;
                                             byte[] hash = md5.ComputeHash(content);
                                             response.Content.Headers.ContentMD5 = hash;
                                             return response;
                                         });
        }
    }
}