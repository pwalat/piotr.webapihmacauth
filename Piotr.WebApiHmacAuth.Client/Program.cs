﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Piotr.WebApiHmacAuth.Web.Infrastructure;

namespace Piotr.WebApiHmacAuth.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var signingHandler = new HmacSigningHandler(new DummySecretRepository(), new CanonicalRepresentationBuilder(),
                                                    new HmacSignatureCalculator());
            signingHandler.Username = "username";

            var client = new HttpClient(new RequestContentMd5Handler()
            {
                InnerHandler = signingHandler
            });
            var response = client.PostAsJsonAsync("http://localhost:48564/api/values", "some content").Result;
            var result = response.Content.ReadAsAsync<string>().Result;

            Console.WriteLine("Server response: "+result);
            Console.ReadLine();
        }
    }
}
